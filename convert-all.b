#!/usr/bin/boron -s
; Convert all SFC models using sfc2gl

idir: %models
odir: %models.glb
dry-run: false

if args [
    dry-run: to-logic find args "-d"
]

ifn dry-run [
    ifn exists? odir [
        make-dir odir
    ]
]
append idir '/'
append odir '/'

foreach mdir read %models [
    if eq? 'h' first mdir [
        infile:  rejoin [idir mdir '/' mdir ".mod"]
        outfile: rejoin [odir mdir '/' mdir ".glb"]
        gdir: join odir mdir
        either dry-run [
            ifn exists? gdir [print ['make-dir gdir]]
            print construct "./sfc2gl < >" ['<' infile '>' outfile]
        ][
            ifn exists? gdir [make-dir gdir]
            execute construct "./sfc2gl < >" ['<' infile '>' outfile]
        ]
    ]
]
