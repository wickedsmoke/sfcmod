/*
  Starfleet Command 1 & 2 Model Reader
  Copyright 2003,2024 Karl Robillard
  SPDX-License-Identifier: GPL-3.0-or-later
*/


#include "sfcmod.h"
#include <assert.h>
#include <cstring>
#include <stdarg.h>
#include <vector>


#ifdef DEBUG_DUMP
#define dprint(...) printf(__VA_ARGS__)
#else
#define dprint(...)
#endif


#if BYTE_ORDER == LITTLE_ENDIAN
#define MAKE_ID(d,c,b,a)    ( ((uint32_t)(a)<<24) | ((uint32_t)(b)<<16) | ((uint32_t)(c)<<8) | (uint32_t)(d) )
#define REVERSE_UNS16(x)    x
#define REVERSE_UNS32(x)    x

#else

#define MAKE_ID(a,b,c,d)    ( ((uint32_t)(a)<<24) | ((uint32_t)(b)<<16) | ((uint32_t)(c)<<8) | (uint32_t)(d) )
#define REVERSE_UNS16(x) (((uint16_t)x >> 8) | (((uint16_t)x & 0x00FF) << 8))
#define REVERSE_UNS32(x) ( ((uint32_t)x >> 24) | (((uint32_t)x & 0x00FF0000L) >> 8) | (((uint32_t)x & 0x0000FF00L) << 8) | (((uint32_t)x & 0x000000FFL) << 24) )
#endif


#define ID_MDLS MAKE_ID('M','D','L','S')
#define ID_STRS MAKE_ID('S','T','R','S')
#define ID_MATS MAKE_ID('M','A','T','S')
#define ID_PNTS MAKE_ID('P','N','T','S')
#define ID_MOD0 MAKE_ID('M','O','D','0')
#define ID_VTXS MAKE_ID('V','T','X','S')
#define ID_POLY MAKE_ID('P','O','L','Y')


int SFCModel::readInt(FILE* fp)
{
    uint32_t i;
    fread(&i, sizeof(uint32_t), 1, fp);
    return REVERSE_UNS32(i);
}


SFCModel::Mesh::Mesh() :
    vertices(nullptr), faces(nullptr),
    vertexCount(0), faceCount(0)
{
}


SFCModel::Mesh::~Mesh()
{
    delete[] vertices;
    delete[] faces;
}


//---------------------------------------------------------------------------


SFCModel::SFCModel() :
    stringPool(nullptr),
    namedPoints(nullptr),
    materials(nullptr),
    meshes(nullptr),
    namedPointCount(0),
    materialCount(0),
    meshCount(0)
{
}


SFCModel::~SFCModel() 
{
    delete[] stringPool;
    delete[] namedPoints;
    delete[] materials;
    delete[] meshes;
}


void SFCModel::_readMaterial(Material* mat, FILE* fp)
{
    mat->flags = readInt(fp);

    fread(&mat->color,          sizeof(Color), 1, fp);
    fread(&mat->ambient,        sizeof(float), 1, fp);
    fread(&mat->diffuse,        sizeof(float), 1, fp);
    fread(&mat->emittance,      sizeof(float), 1, fp);
    fread(&mat->diffuseTexture, sizeof(Material::TextureInfo), 1, fp);

    Material::TextureInfo* lumi = &mat->luminanceTexture;
    if (mat->flags & Material::kLuminance) {
        fread(lumi, sizeof(Material::TextureInfo), 1, fp);
    } else {
        lumi->texID = lumi->texNameOffset = 0;
    }
}


void SFCModel::_readChunk(SFCModel::LoadState* ls, FILE* fp, const long kEndPos)
{
    while (! feof(fp))
    {
        long pos = ftell(fp);
        if(pos >= kEndPos)
            return;

        struct tChunk
        {
            uint32_t fType;
            int32_t  fSize;
        } chunk;

        chunk.fType = readInt(fp);
        chunk.fSize = readInt(fp);
        pos = ftell(fp);

        switch (chunk.fType)
        {
        case ID_MDLS:
            {
                fread(&flatHeader, sizeof(FlatHeader), 1, fp);
                dprint("MDLS - Model file"
                       " (ver: %d radius: %g lods: %d hysteresis: %g)\n",
                        flatHeader.version,
                        flatHeader.radius,
                        flatHeader.totalLODs,
                        flatHeader.LODHysteresis);

                meshes = new Mesh[ flatHeader.totalLODs ];
                meshCount = 0;

                fread(&lodTransitions, sizeof(float), kMaxLOD, fp);
                _readChunk(ls, fp, pos + chunk.fSize);
            }
            break;

        case ID_STRS:
            dprint("STRS - Strings (%d bytes)\n", chunk.fSize);
            delete stringPool;
            stringPool = new char[ chunk.fSize ];
            fread(stringPool, sizeof(char), chunk.fSize, fp);
            break;

        case ID_MATS:
            {
                dprint("MATS - Materials\n");

                // Materials are no longer uniform in size.
                // Some may contain luminance maps others may not.
                std::vector<Material> matArray;
                const long kStart = ftell(fp);    // be back

                while (ftell(fp) - kStart < chunk.fSize) {
                    Material material;
                    _readMaterial(&material, fp);
                    matArray.push_back(material);
                    dprint("    (flg:0x%x col:%d,%d,%d %f,%f,%f"
                           " tex:%d,%d ltex:%d,%d)\n",
                        material.flags,
                        material.color.r, material.color.g, material.color.g,
                        material.ambient, material.diffuse, material.emittance,
                        material.diffuseTexture.texID,
                        material.diffuseTexture.texNameOffset,
                        material.luminanceTexture.texID,
                        material.luminanceTexture.texNameOffset);
                }

                // Now copy our vector into the array.
                materialCount = matArray.size();
                if (materialCount) {
                    materials = new Material[ materialCount ];
                    if (materials) {
                        memcpy(materials, matArray.data(),
                               materialCount * sizeof(Material));
                    }
                } else
                    materials = nullptr;
            }
            break;

        case ID_PNTS:
            namedPointCount = chunk.fSize / sizeof(NamedPoint);
            dprint("PNTS - Named points (%d)\n", namedPointCount);
            if (namedPointCount) {
                namedPoints = new NamedPoint[ namedPointCount ];
                fread(namedPoints, sizeof(NamedPoint), namedPointCount, fp);
            }
            break;

        case ID_MOD0:
            if (meshes) {
                ++ls->curMesh;
                Mesh* mesh = meshes + ls->curMesh;

                assert(meshCount < flatHeader.totalLODs);
                ++meshCount;

                mesh->vertexCount = readInt(fp);
                mesh->faceCount   = readInt(fp);
                mesh->vertices    = new Point3[ mesh->vertexCount ];
                mesh->faces       = new FaceDef[ mesh->faceCount ];

                ls->curFace = mesh->faces;

                dprint("MOD0 - Geometry (vc:%d fc:%d)\n",
                       mesh->vertexCount, mesh->faceCount);

                _readChunk(ls, fp, pos + chunk.fSize);
            }
            break;

        case ID_VTXS:
            dprint("VTXS - Vertex\n");
            if (ls->curMesh >= 0) {
                Mesh* mesh = meshes + ls->curMesh;
                fread(mesh->vertices, sizeof(Point3), mesh->vertexCount, fp);
            }
            break;

        case ID_POLY:
            if (ls->curFace) {
                FaceDef* face = ls->curFace;
                ls->curFace++;

                face->vtxCount      = readInt(fp);
                face->materialIndex = readInt(fp);
                dprint("POLY (vtx:%d mat:%d)\n",
                        face->vtxCount, face->materialIndex);
                assert(face->vtxCount <= 3);

                fread(&face->normal, sizeof(Point3), 1, fp);
                fread(face->vtx, sizeof(FlatPolyVertex), face->vtxCount, fp);
            }
            break;

        default:
            fprintf(stderr, "Unknown or invalid chunk\n");
            break;
        }

        // Advance to end of data (which is start of next chunk)
        // Note: Data blocks are padded to end on a dword boundary
        //
        const long kNextPos = ((pos + chunk.fSize) + 3) & ~3;
        fseek(fp, kNextPos, SEEK_SET);
    }
}


#if 0
void SFCModel::_splitBreakUpMeshes()
{
    Mesh* newList = 0, *curMesh = 0;

    // Counting and new list creation loop
    Mesh* mesh = firstMesh;
    if (mesh) {
        newList = curMesh = new Mesh;
    }

    while (mesh)
    {
        unsigned int v_start = 0, v_stop = 0, f_start = 0;
        for (int i = 0; i < mesh->faceCount; i++)
        {
            int j;
            for (j = 0; j < mesh->faces[i].vtxCount; j++) {
                if (v_start > mesh->faces[i].vtx[j].vtxIndex)
                    v_start = mesh->faces[i].vtx[j].vtxIndex;

                if (v_stop < mesh->faces[i].vtx[j].vtxIndex)
                    v_stop = mesh->faces[i].vtx[j].vtxIndex;
            }

            // Add new group
            if (i + 1 >= mesh->faceCount ||
                mesh->faces[i+1].vtx[0].flags != mesh->faces[i].vtx[0].flags)
            {
                // Load the group with the mesh data
                curMesh->faceCount = i - f_start + 1;
                curMesh->faces = new FaceDef[curMesh->faceCount];
                curMesh->vertexCount = v_stop - v_start + 1;
                curMesh->vertices = new Point3[curMesh->vertexCount];

                for (j = 0; j < curMesh->vertexCount; j++) {
                    curMesh->vertices[j] = mesh->vertices[ v_start + j ];
                }

                for (j = 0; j < curMesh->faceCount; j++) {
                    curMesh->faces[j] = mesh->faces[ f_start + j ];
                    for (int k = 0; k < curMesh->faces[j].vtxCount; k++) {
                        curMesh->faces[j].vtx[k] = mesh->faces[ f_start + j ].vtx[k];
                        curMesh->faces[j].vtx[k].vtxIndex -= v_start;
                    }
                }

                v_start = v_stop + 1;
                v_stop = v_start;
                f_start = i + 1;
                if (i + 1 < mesh->faceCount || mesh->next) {
                    curMesh->next = new Mesh;
                    curMesh = curMesh->next;
                }
            }
        }
        mesh = mesh->next;
    }

    // Remove old mesh contents
    curMesh = firstMesh;
    while (curMesh) {
        firstMesh = curMesh->next;
        delete curMesh;
        curMesh = firstMesh;
    }

    firstMesh = newList;
}
#endif


bool SFCModel::loadModel(const char* filename)
{
    FILE* fp = fopen(filename, "rb");
    if (fp) {
        LoadState ls;
        long size;

        fseek(fp, 0, SEEK_END);
        size = ftell(fp);
        fseek(fp, 0, SEEK_SET);

        ls.curFace = nullptr;
        ls.curMesh = -1;
        _readChunk(&ls, fp, size);

        fclose(fp);

        //_splitBreakUpMeshes();
        return true;
    }
    return false;
}


#ifdef UNIT_TEST
// g++ -DUNIT_TEST -DDEBUG_DUMP sfcmod.cpp
int main(int argc, char** argv)
{
    SFCModel mod;
    if (argc > 1)
        mod.loadModel(argv[1]);
    return 1;
}
#endif
