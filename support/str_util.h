#ifndef STR_UTIL_H
#define STR_UTIL_H
#ifdef __cplusplus
extern "C" {
#endif

char* strncpy0(char* dest, const char* src, size_t n);
char* str_replaceFile(char* path, int pathLen, const char* file);

#ifdef __cplusplus
}
#endif
#endif
