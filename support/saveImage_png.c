/*
 * Image32 PNG Saver
 * Copyright (C) 2024  Karl Robillard
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <png.h>
#include "image32.h"

/**
 * Saves a PNG file with the libpng library.
 *
 * \param image  Pointer to initialized Image32 struct.
 *
 * \return Zero if saving failed.
 */
int saveImage_png(FILE* fp, const Image32* image, int invert)
{
    png_structp png;
    png_infop info;
    png_bytep row_pointer;
    int bpl, i;

    png = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (! png)
        return 0;

    info = png_create_info_struct(png);
    if (! info) {
        png_destroy_write_struct(&png, NULL);
        return 0;
    }

    if (setjmp(png_jmpbuf(png))) {
        png_destroy_write_struct(&png, &info);
        return 0;
    }

    png_init_io(png, fp);   /* Using standard C streams */

    png_set_IHDR(png, info, image->w, image->h, 8, PNG_COLOR_TYPE_RGB_ALPHA,
                 PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT,
                 PNG_FILTER_TYPE_DEFAULT);

    png_write_info(png, info);

    bpl = image->w * 4;
    row_pointer = (png_byte*) image->pixels;
    if (invert) {
        row_pointer += (image->h - 1) * bpl;
        bpl = -bpl;
    }

    for (i = 0; i < image->h; i++) {
        png_write_row(png, row_pointer);
        row_pointer += bpl;
    }

    png_write_end(png, info);
    png_destroy_write_struct(&png, &info);
    return 1;
}
