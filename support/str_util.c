#include <stddef.h>

/*
 * Works as strncpy but with guaranteed termination.
 */
char* strncpy0(char* dest, const char *src, size_t n)
{
   size_t last = n - 1;
   size_t i;
   for (i = 0; i < last && src[i] != '\0'; i++)
       dest[i] = src[i];
   for ( ; i < n; i++)
       dest[i] = '\0';
   return dest;
}

/*
 * Replace last path node with a new file name.
 *
 * \param path  Must be large enough to hold new string.
 *
 * \return Position in path where new file was copied.
 */
char* str_replaceFile(char* path, int pathLen, const char* file)
{
    char* it = path + pathLen;
    int ch;
    while (it != path) {
        --it;
        ch = *it;
        if (ch == '/' || ch == '\\') {
            ++it;
            break;
        }
    }
    path = it;
    while (*file)
        *it++ = *file++;
    *it = '\0';
    return path;
}
