/*
  Starfleet Command 1 & 2 Model Reader
  Copyright 2003,2024 Karl Robillard
  SPDX-License-Identifier: GPL-3.0-or-later
*/

#ifndef SFCMOD_H
#define SFCMOD_H

#include <stdio.h>
#include <stdint.h>

class SFCModel
{
public:

    SFCModel();
    ~SFCModel();

    bool loadModel( const char* filename );

    enum
    {
        kMaxLOD = 5
    };

    struct FlatHeader
    {
        uint32_t version;
        float radius;
        int32_t totalLODs;
        float LODHysteresis;
    } flatHeader;

    struct Point3
    {
        float x, y, z;
    };

    struct NamedPoint
    {
        Point3 fPos;
        uint32_t fNameOffset;
    };

    struct Color
    {
        uint8_t r;
        uint8_t g;
        uint8_t b;
        uint8_t pad;
    };

    struct Material
    {
        enum
        {
            kTexMapped = 0x0001,    // Texture mapping
            kGouraud   = 0x0002,    // Gouraud (smooth) shading
            kFlat      = 0x0004,    // Flat shading
            kWire      = 0x0008,    // Polygon should be wire frame
            kAdditive  = 0x0010,    // Polygon should be blended
            kLuminance = 0x0020,    // Has luminance texture maps
        };

        struct TextureInfo
        {
            int texID;
            int texNameOffset;
        };

        unsigned int flags;
        Color color;
        float ambient;
        float diffuse;
        float emittance;

        TextureInfo diffuseTexture;
        TextureInfo luminanceTexture;
    };

    struct FlatPolyVertex
    {
        uint16_t vtxIndex;
        uint16_t flags;
        Point3 normal;
        float u;
        float v;
        float light;
    };

    struct FaceDef
    {
        int vtxCount;
        int materialIndex;
        Point3 normal;
        FlatPolyVertex vtx[3];
    };

    struct Mesh
    {
        Mesh();
        ~Mesh();

        Point3* vertices;
        FaceDef* faces;
        int vertexCount;
        int faceCount;

        //unsigned int*    fSmoothing;
    };

    char* stringPool;
    NamedPoint* namedPoints;
    Material* materials;
    Mesh* meshes;

    int namedPointCount;
    int materialCount;
    int meshCount;
    float lodTransitions[ kMaxLOD ];

private:

    struct LoadState {
        FaceDef* curFace;
        int curMesh;
    };

    void _readMaterial(Material* material, FILE* file);
    void _readChunk(LoadState*, FILE*, const long kEndPos);
    //void _splitBreakUpMeshes();
    static int readInt(FILE*);
};


#endif  //SFCMOD_H
