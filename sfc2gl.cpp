/*
  Starfleet Command 1 & 2 Model Converter
  Copyright 2024 Karl Robillard
  SPDX-License-Identifier: GPL-3.0-or-later
*/

#include <cstdio>
#include <cstring>
#include <math.h>
#include <stdlib.h>
#include "bmp.h"
#include "glb.h"
#include "image32.h"
#include "sfcmod.cpp"
#include "str_util.h"

extern "C" int saveImage_png(FILE* fp, const Image32* image, int invert);

// sysexits.h
#define EX_USAGE        64  /* command line usage error */
#define EX_DATAERR      65  /* data format error */
#define EX_NOINPUT      66  /* cannot open input */
#define EX_CANTCREAT    73  /* can't create (user) output file */

#define APP_VERSION "sfc2gl 1.0"


const int MAX_MATERIAL = 32;

struct FaceStat {
    uint8_t matUsed[MAX_MATERIAL];
    uint8_t matMap[MAX_MATERIAL];
    int groupCount;
    int matCount;
    int matHigh;
    int triCount;
    int polyCount;
};

static void faceStatistics(const SFCModel::Mesh* mesh, FaceStat* stat)
{
    const SFCModel::FaceDef* fit = mesh->faces;
    const SFCModel::FaceDef* fend = fit + mesh->faceCount;
    uint8_t* matUsed = stat->matUsed;
    int i;
    int high = -1;
    int curMaterial = -1;
    int mcount = 0;
    int groups = 0;
    int tris = 0;
    int nonTris = 0;

    memset(matUsed, 0, MAX_MATERIAL);

    for (; fit != fend; ++fit) {
        i = fit->materialIndex;
        if (! matUsed[i]) {
            matUsed[i] = 1;
            if (i > high)
                high = i;
            ++mcount;
        }
        if (curMaterial != i) {
            curMaterial = i;
            ++groups;
        }

        if (fit->vtxCount == 3)
            ++tris;
        else
            ++nonTris;
    }

    stat->groupCount = groups;
    stat->matCount = mcount;
    stat->matHigh = high;
    stat->triCount = tris;
    stat->polyCount = nonTris;

    //printf("KR grp:%d mat:%d high:%d\n", groups, mcount, high);
}

#define v3_lengthSq(A)  (A[0]*A[0] + A[1]*A[1] + A[2]*A[2])

float v3_normalize(float* vec)
{
    float len = sqrtf(v3_lengthSq(vec));
    if (len > 0.0f) {
        float inv = 1.0f / len;
        vec[0] *= inv;
        vec[1] *= inv;
        vec[2] *= inv;
    }
    return len;
}

/*
 * Allocate 8-byte aligned blocks of memory with a single malloc() call.
 *
 * Caller must free() ptrs[0].
 */
static size_t mallocBlocks(size_t* sizes, void** ptrs, int count)
{
    int i;
    size_t total = 0;
    for (i = 0; i < count; ++i) {
        sizes[i] = (sizes[i] + 7) & ~3;
        total += sizes[i];
    }

    char* buf = (char*) malloc(total);
    for (i = 0; i < count; ++i) {
        ptrs[i] = buf;
        buf += sizes[i];
    }
    return total;
}

static const float* matchAttributes(const float* buf, const float* bufEnd,
                                    int stride, const float* pattern)
{
    size_t len = sizeof(float) * stride;
    while (buf != bufEnd) {
        if (memcmp(pattern, buf, len) == 0)
            return buf;
        buf += stride;
    }
    return NULL;
}

struct MaterialData {
    Image32 img;
    char* imgFileDiffuse;
    char* imgFileLumi;
    float color[3];
    float imageHf;
    float atlasYf;
    int atlasY;
};

#ifdef __linux
#include "processDir.h"

struct FindFileICData {
    char* inputFilePos;
    char nameIC[40];
};

// Match filename ignoring case.
static int findFileIC(const char* name, int type, void* user)
{
    FindFileICData* ffdat = (FindFileICData*) user;
    if (type != PDIR_DIR && strcasecmp(name, ffdat->nameIC) == 0) {
        strcpy(ffdat->inputFilePos, name);
        return PDIR_STOP;
    }
    return PDIR_CONTINUE;
}
#endif

bool gVerbose = false;

static void scaleX2(Image32* img)
{
    Image32 img2;
    image32_allocPixels(&img2, img->w * 2, img->h * 2);

    const uint32_t* src = img->pixels;
    const uint32_t* send;
    uint32_t* drow = img2.pixels;
    uint32_t* dest;
    uint32_t pix;
    size_t drowBytes = img2.w * sizeof(uint32_t);

    for (int y = 0; y < img->h; ++y) {
        dest = drow;
        send = src + img->w;
        while (src != send) {
            pix = *src++;
            *dest++ = pix;
            *dest++ = pix;
        }
        memcpy(drow + img2.w, drow, drowBytes);
        drow += img2.w * 2;
    }

    image32_freePixels(img);
    *img = img2;
}

/*
 * Return atlas height.
 */
static int buildAtlas(char* inputPath, char* inputFilePos,
                      const char* textureFile,
                      MaterialData* matd, int count, bool illum)
{
    Image32 atlas;
    int iw, ih;
    int atlasW = 0, atlasH = 0;
    const char* basename;
    MaterialData* it;
    MaterialData* end = matd + count;
    bool needUpscale = false;

#ifdef __linux
    FindFileICData ffdat;
    ffdat.inputFilePos = inputFilePos;
#endif

    for (it = matd; it != end; ++it) {
        image32_init(&it->img);
        basename = illum ? it->imgFileLumi: it->imgFileDiffuse;
        if (basename) {
#ifdef __linux
            sprintf(ffdat.nameIC, "%s.bmp", basename);
            *inputFilePos = '\0';
            processDir(inputPath, findFileIC, &ffdat);
#else
            sprintf(inputFilePos, "%s.bmp", basename);
#endif
            if (gVerbose)
                printf("loadBMP %s\n", inputPath);
            if (loadBMP(inputPath, (uint8_t**)&it->img.pixels, &iw, &ih) == 0) {
                if (iw > atlasW) {
                    if (atlasW)
                        needUpscale = true;
                    atlasW = iw;
                }

                it->img.w = iw;
                it->img.h = ih;
                it->imageHf = (float) ih;
                it->atlasYf = (float) atlasH;
                it->atlasY = atlasH;

                atlasH += ih;
            }
        }
    }

    if (needUpscale) {
        int halfW = atlasW / 2;
        if (gVerbose) {
            printf("Upscaling %s images to atlas width %d.\n",
                   illum ? "luminance" : "diffuse",
                   atlasW);
        }

        atlasH = 0;
        for (it = matd; it != end; ++it) {
            if (it->img.w == halfW) {
                scaleX2(&it->img);
                it->imageHf = (float) it->img.h;
            } else if (it->img.w != atlasW) {
                fprintf(stderr,
                        "Image width %d doesn't match atlas width %d.\n",
                        it->img.w, atlasW);
            }
            it->atlasYf = (float) atlasH;
            it->atlasY = atlasH;

            atlasH += it->img.h;
        }
    }

    if (image32_allocPixels(&atlas, atlasW, atlasH)) {
        for (it = matd; it != end; ++it) {
            if (it->img.pixels) {
                image32_blit(&atlas, 0, it->atlasY, &it->img, 0);
                image32_freePixels(&it->img);
            }
        }

        FILE* fp = fopen(textureFile, "wb");
        if (fp) {
            saveImage_png(fp, &atlas, 0);
            fclose(fp);
        }
        image32_freePixels(&atlas);
    }

    return atlasH;
}

static const char glbScene[] =
    "\"scene\": 0,\n"
    "\"scenes\": [{\"nodes\": [0]}],\n"
    "\"nodes\": [{\"mesh\": 0}],\n"
    "\"meshes\": [{\n"
    "  \"primitives\": [{\n"
    "    \"attributes\": {\"POSITION\": 1, \"NORMAL\": 2, \"TEXCOORD_0\": 3},\n"
    "    \"indices\": 0,\n"
    "    \"material\": 0\n"
    "  }]\n"
    "}],\n"
    ;

static const char glbMaterials[] =
    "\"materials\": [{\n"
    "  \"pbrMetallicRoughness\": {\n"
    "    \"baseColorFactor\": [%f, %f, %f, 1.0],\n"
    "    \"baseColorTexture\": {\"index\": 0},\n"
    "    \"metallicFactor\": 0.5,\n"
    "    \"roughnessFactor\": 0.1\n"
    "  }%s\n"
    "}],\n"
    "\"samplers\": [{\n"
    "  \"magFilter\": 9729, \"minFilter\": 9987,\n"
    "  \"wrapS\": 33648, \"wrapT\": 33648\n"
    "}],\n"
    ;

static const char glbEmissive[] =
    ",\n  \"emissiveFactor\": [1.0, 1.0, 1.0]"
    ",\n  \"emissiveTexture\": {\"index\": 1}";

static const char glbTextures[] =
    "\"textures\": [{\"source\": 0, \"sampler\": 0}],\n"
    "\"images\": [{\"uri\": \"%s\"}],\n";

static const char glbTextures2[] =
    "\"textures\": [\n"
    "  {\"source\": 0, \"sampler\": 0},\n"
    "  {\"source\": 1, \"sampler\": 0}\n"
    "],\n"
    "\"images\": [{\"uri\": \"%s\"}, {\"uri\": \"%s\"}],\n";

static const char glbAccessors[] =
    "\"accessors\": [{\n"
    "  \"bufferView\": 0, \"componentType\": 5123, \"type\": \"SCALAR\",\n"
    "  \"count\": %ld,\n"
    "  \"min\": [%d],\n"
    "  \"max\": [%d]\n"
    "  },{\n"
    "  \"bufferView\": 1, \"componentType\": 5126, \"type\": \"VEC3\",\n"
    "  \"count\": %ld,\n"
    "  \"min\": [%.10f, %.10f, %.10f],\n"
    "  \"max\": [%.10f, %.10f, %.10f]\n"
    "  },{\n"
    "  \"bufferView\": 1, \"componentType\": 5126, \"type\": \"VEC3\",\n"
    "  \"byteOffset\": 12,\n"
    "  \"count\": %ld\n"
    "  },{\n"
    "  \"bufferView\": 1, \"componentType\": 5126, \"type\": \"VEC2\",\n"
    "  \"byteOffset\": 24,\n"
    "  \"count\": %ld\n"
    "}],\n"
    ;

struct WorkBuffers {
    MaterialData* material;
    float* attr;
    uint16_t* indices;
    char* inPath;
    char* outPath;
};

/*
 * Return error message or NULL if successful.
 */
static const char* sfc2gltf(const SFCModel* smod, int /*lod*/,
                            const char* filename,
                            const char* inputFile)
{
    const int BLK_COUNT = sizeof(WorkBuffers) / sizeof(void*);
    const int ASTRIDE = 8;
    const size_t vec2Len = sizeof(float) * 2;
    const size_t vec3Len = sizeof(float) * 3;
    const char* error = NULL;
    SFCModel::Mesh* smesh = smod->meshes;
    FaceStat stat;
    char textureFile[32];
    char textureFileLumi[32];
    float atlasHf;
    float vmin[3], vmax[3];
    uint16_t indMin, indMax;
    size_t vtxCount;
    size_t bsize[BLK_COUNT];
    WorkBuffers bptr;

    faceStatistics(smesh, &stat);
    if (stat.polyCount)
        return "Non-triangular faces are not supported";

    glb_vec3Bounds((float*) smesh->vertices, smesh->vertexCount, 3, vmin, vmax);

    bsize[0] = sizeof(MaterialData) * stat.matCount;
    bsize[1] = sizeof(float) * ASTRIDE * 3 * stat.triCount;
    bsize[2] = sizeof(uint16_t) * 3 * stat.triCount;
    bsize[3] = 1024;
    bsize[4] = 1024;

    // Allocate a single working buffer for everything.
    mallocBlocks(bsize, (void**) &bptr, BLK_COUNT);
    if (! bptr.material)
        return "Cannot allocate work buffer";

    // Convert materials.
    {
    MaterialData* matd = bptr.material;
    int dtexCount = 0;
    int ltexCount = 0;
    int j;

    for (int i = 0; i <= stat.matHigh; ++i) {
        if (! stat.matUsed[i]) {
            stat.matMap[i] = 0;
            continue;
        }

        SFCModel::Material* smat = smod->materials + i;

        // Merge duplicates.
        for (j = 0; j < i; ++j) {
            if (stat.matUsed[j]) {
                SFCModel::Material* prev = smod->materials + j;
                if (memcmp(smat, prev, sizeof(SFCModel::Material)) == 0)
                    break;
            }
        }
        if (j < i) {
            stat.matMap[i] = stat.matMap[j];
            continue;
        }

        stat.matMap[i] = matd - bptr.material;

        matd->color[0] = (float) smat->color.r / 255.0f;
        matd->color[1] = (float) smat->color.g / 255.0f;
        matd->color[2] = (float) smat->color.b / 255.0f;
        matd->imgFileDiffuse =
        matd->imgFileLumi = NULL;

        if (smat->flags & SFCModel::Material::kTexMapped) {
            matd->imgFileDiffuse = smod->stringPool +
                                   smat->diffuseTexture.texNameOffset;
            ++dtexCount;
        }
        if (smat->flags & SFCModel::Material::kLuminance) {
            matd->imgFileLumi = smod->stringPool +
                                smat->luminanceTexture.texNameOffset;
            ++ltexCount;
        }
        ++matd;
    }

    textureFileLumi[0] = 0;

    //printf("KR dtex:%d\n", dtexCount);
    if (dtexCount) {
        char basename[32];
        char* inPos;
        char* outPos;

        strcpy(bptr.inPath, inputFile);
        inPos = str_replaceFile(bptr.inPath, strlen(bptr.inPath), "NA");

        strcpy(bptr.outPath, filename);
        outPos = str_replaceFile(bptr.outPath, strlen(bptr.outPath), "NA");

        {
        strcpy(basename, inputFile + (inPos - bptr.inPath));
        char* cp = strchr(basename, '.');
        if (cp)
            *cp = '\0';
        }

        if (ltexCount == dtexCount) {
            sprintf(outPos, "%s_i.png", basename);
            strcpy(textureFileLumi, outPos);
            buildAtlas(bptr.inPath, inPos, bptr.outPath,
                       bptr.material, ltexCount, true);
        } else if (ltexCount) {
            fprintf(stderr,
            "Warning: Luminance texture count doesn't match diffuse count\n");
        }

        sprintf(outPos, "%s_d.png", basename);
        strcpy(textureFile, outPos);
        atlasHf = (float) buildAtlas(bptr.inPath, inPos, bptr.outPath,
                                     bptr.material, dtexCount, false);
    } else {
        strcpy(textureFile, "unset.png");
        atlasHf = 0.0f;
    }
    }

    // Convert geometry.
    {
    const SFCModel::FaceDef* fit = smesh->faces;
    const SFCModel::FaceDef* fend = fit + smesh->faceCount;
    float* attr = bptr.attr;
    float* attrEnd = attr;
    const float* match;
    uint16_t* ind = bptr.indices;
    MaterialData* matd;

#define FLIP_FACES

    for (; fit != fend; ++fit) {
#ifdef FLIP_FACES
        for (int c = 2; c >= 0; --c)
#else
        for (int c = 0; c < 3; ++c)
#endif
        {
            const SFCModel::FlatPolyVertex* pv = fit->vtx + c;
            memcpy(attrEnd, smesh->vertices + pv->vtxIndex, vec3Len);
            memcpy(attrEnd + 3, &pv->normal, vec3Len);
            memcpy(attrEnd + 6, &pv->u, vec2Len);
            v3_normalize(attrEnd + 3);

            // Remap UVs for atlas.
            if (atlasHf) {
                matd = bptr.material + stat.matMap[ fit->materialIndex ];
                float texV = attrEnd[7];
                texV *= matd->imageHf / atlasHf;
                attrEnd[7] = texV + matd->atlasYf / atlasHf;
            }

            //printf("norm %f,%f,%f\n",
            //        pv->normal.x, pv->normal.y, pv->normal.z);
            //printf("uv %f,%f,%f\n", pv->u, pv->v, pv->light);

            match = matchAttributes(attr, attrEnd, ASTRIDE, attrEnd);
            if (! match) {
                match = attrEnd;
                attrEnd += ASTRIDE;
            }
            *ind++ = (match - attr) / ASTRIDE;
        }
    }

    vtxCount = (attrEnd - attr) / ASTRIDE;
    //vtxCount = smesh->vertexCount;
    indMin = 0;
    indMax = vtxCount - 1;
    }

    FILE* fp = fopen(filename, "wb");
    if (! fp) {
        error = "Cannot open .glb file";
    } else {
        size_t indCount = smesh->faceCount * 3;
        size_t indLen = indCount * sizeof(uint16_t);
        size_t vtxLen = vtxCount * ASTRIDE * sizeof(float);

        error = glb_writeHeader(fp, APP_VERSION);
        if (! error) {
            fputs(glbScene, fp);

            MaterialData* matd = bptr.material;
            fprintf(fp, glbMaterials,
                    matd->color[0], matd->color[1], matd->color[2],
                    textureFileLumi[0] ? glbEmissive : "");

            if (textureFileLumi[0])
                fprintf(fp, glbTextures2, textureFile, textureFileLumi);
            else
                fprintf(fp, glbTextures, textureFile);

            fprintf(fp, glbAccessors, indCount, indMin, indMax,
                    vtxCount, vmin[0], vmin[1], vmin[2],
                              vmax[0], vmax[1], vmax[2],
                    vtxCount, vtxCount);

            glb_jsonBuffersIV(fp, indLen, vtxLen, ASTRIDE * sizeof(float));

            // Write binary chunk.
            void* binParts[2];
            size_t binPartsLen[2];
            binParts[0] = bptr.indices;
            binParts[1] = bptr.attr;    //smesh->vertices;
            binPartsLen[0] = indLen;
            binPartsLen[1] = vtxLen;

            error = glb_writeBin(fp, binParts, binPartsLen, 2);
        }
        fclose(fp);
    }

    free(bptr.material);
    return error;
}

void usage(char** argv)
{
    printf("Usage: %s [OPTIONS] <input.mod> <output.glb>\n\n"
           "Options:\n"
           "  -h         Print this help and exit.\n"
           "  -v         Verbose output.\n"
           "  --version  Print version and exit.\n",
           argv[0]);
}

int main(int argc, char** argv)
{
    const char* infile  = NULL;
    const char* outfile = NULL;
    //char format = 'g';

#define ARG_EQU(S)  (strcmp(arg, S) == 0)

    for (int i = 1; i < argc; ++i) {
        const char* arg = argv[i];
        if (arg[0] == '-') {
            if (arg[1] == 'v')
                gVerbose = true;
            else if (arg[1] == 'h') {
                usage(argv);
                return 0;
            } else if (ARG_EQU("--version")) {
                printf(APP_VERSION "\n");
                return 0;
            } else {
                fprintf(stderr, "Invalid option %s\n", arg);
                return EX_USAGE;
            }
        } else if (infile) {
            outfile = arg;
        } else {
            infile = arg;
        }
    }

    if (! outfile) {
        usage(argv);
        return EX_USAGE;
    }

    SFCModel mod;
    if (! mod.loadModel(infile)) {
        fprintf(stderr, "Cannot load SFC model %s\n", infile);
        return EX_NOINPUT;
    }

    //if (format == 'g')
    {
        const char* error = sfc2gltf(&mod, 0, outfile, infile);
        if (error) {
            fprintf(stderr, "%s (%s)\n", error, outfile);
            return EX_CANTCREAT;
        }
    }

    return 0;
}
