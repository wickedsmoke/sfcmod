exe %sfc2gl [
    include_from [%. %support]
    libs [%png]
    sources [
        %sfc2gl.cpp
        %support/bmp.c
        %support/glb.c
        %support/image32.c
        %support/processDir.c
        %support/saveImage_png.c
        %support/str_util.c
    ]
]

exe %sfcmod [
    cflags "-DUNIT_TEST -DDEBUG_DUMP"
    sources [%sfcmod.cpp]
]
